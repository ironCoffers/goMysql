package goMySQL

import (
	"fmt"
	"reflect"
	"testing"
)

func ExampleOperator() {
	fmt.Println(EQ.String())
	fmt.Println(NEQ.String())
	fmt.Println(GT.String())
	fmt.Println(EGT.String())
	fmt.Println(LT.String())
	fmt.Println(ELT.String())
	// Output:
	//=
	//<>
	//>
	//>=
	//<
	//<=
}

func TestExpr(t *testing.T) {
	got := &Expr{
		Field: "id",
		Op:    EQ,
		Value: 1,
	}
	want := "id = ?"
	if got.String() != want {
		t.Errorf("got '%s' want '%s'", got.String(), want)
	}
}

func TestExprNode(t *testing.T) {
	got := &ExprNode{
		Node: &Expr{
			Field: "id",
			Op:    EQ,
			Value: 1,
		},
	}
	want := "id = ?"
	wantValue := 1
	if got.String() != want {
		t.Errorf("got '%q' want '%q'", got.String(), want)
	}
	if got.Value() != wantValue {
		t.Errorf("got '%v' want '%v'", got.Value(), wantValue)
	}
}

func TestExpression(t *testing.T) {
	checkExpression := func(t *testing.T, got *Expression, want string, wantValue []interface{}) {
		t.Helper()
		if got.String() != want {
			t.Errorf("got '%q' want '%q'", got, want)
		}
		if !reflect.DeepEqual(got.Value(), wantValue) {
			t.Errorf("got '%v' want '%v'", got.Value(), wantValue)
		}
	}
	t.Run("test of addExpr", func(t *testing.T) {
		got := &Expression{}
		node := &Expr{
			Field: "id",
			Op:    EQ,
			Value: 1,
		}
		got.AddExpr(node)
		want := "id = ?"
		wantValue := []interface{}{1}
		checkExpression(t, got, want, wantValue)
	})
	t.Run("test of more AddExpr", func(t *testing.T) {
		got := &Expression{}
		exprs := []*Expr{
			{
				Field: "id",
				Op:    EQ,
				Value: 1,
			},
			{
				Field: "name",
				Op:    EQ,
				Value: "frf",
			},
			{
				Field: "age",
				Op:    GT,
				Value: 18,
			},
		}
		for _, expr := range exprs {
			got.AddExpr(expr)
		}
		want := "id = ? AND name = ? AND age > ?"
		wantValue := []interface{}{1, "frf", 18}
		checkExpression(t, got, want, wantValue)
	})

	t.Run("test of AndExpr", func(t *testing.T) {
		got := &Expression{}
		expr := &Expr{
			Field: "id",
			Op:    EQ,
			Value: 1,
		}
		got.AndExpr(expr.Field, expr.Op, expr.Value)
		want := "id = ?"
		wantValue := []interface{}{1}
		checkExpression(t, got, want, wantValue)
	})

	t.Run("test of more AndExpr", func(t *testing.T) {
		got := &Expression{}
		exprs := []*Expr{
			{
				Field: "id",
				Op:    EQ,
				Value: 1,
			},
			{
				Field: "name",
				Op:    EQ,
				Value: "frf",
			},
			{
				Field: "age",
				Op:    GT,
				Value: 18,
			},
		}
		for _, expr := range exprs {
			got.AndExpr(expr.Field, expr.Op, expr.Value)
		}
		want := "id = ? AND name = ? AND age > ?"
		wantValue := []interface{}{1, "frf", 18}
		checkExpression(t, got, want, wantValue)
	})

	t.Run("test of OrExpr", func(t *testing.T) {
		got := &Expression{}
		expr := &Expr{
			Field: "id",
			Op:    EQ,
			Value: 1,
		}
		got.OrExpr(expr.Field, expr.Op, expr.Value)
		want := "id = ?"
		wantValue := []interface{}{1}
		checkExpression(t, got, want, wantValue)
	})

	t.Run("test of more OrExpr", func(t *testing.T) {
		got := &Expression{}
		exprs := []*Expr{
			{
				Field: "id",
				Op:    EQ,
				Value: 1,
			},
			{
				Field: "name",
				Op:    EQ,
				Value: "frf",
			},
			{
				Field: "age",
				Op:    GT,
				Value: 18,
			},
		}
		for _, expr := range exprs {
			got.OrExpr(expr.Field, expr.Op, expr.Value)
		}
		want := "id = ? OR name = ? OR age > ?"
		wantValue := []interface{}{1, "frf", 18}
		checkExpression(t, got, want, wantValue)
	})

	t.Run("test of AndExpr and OrExpr", func(t *testing.T) {
		got := &Expression{}
		testCases := []struct {
			expr     *Expr
			relation Relation
		}{
			{
				expr: &Expr{
					Field: "id",
					Op:    EQ,
					Value: 1,
				},
				relation: And,
			},
			{
				expr: &Expr{
					Field: "name",
					Op:    EQ,
					Value: "frf",
				},
				relation: Or,
			},
			{
				expr: &Expr{
					Field: "age",
					Op:    GT,
					Value: 18,
				},
				relation: And,
			},
		}
		for _, testCase := range testCases {
			switch testCase.relation {
			case And:
				got.AndExpr(testCase.expr.Field, testCase.expr.Op, testCase.expr.Value)
			case Or:
				got.OrExpr(testCase.expr.Field, testCase.expr.Op, testCase.expr.Value)
			}
		}
		want := "id = ? OR name = ? AND age > ?"
		wantValue := []interface{}{1, "frf", 18}
		checkExpression(t, got, want, wantValue)
	})
	t.Run("test of clause", func(t *testing.T) {
		got := &Expression{}
		clause := "id = 1"
		err := got.AddClause(clause)
		if err != nil {
			t.Error(err)
		}
		want := "id = ?"
		wantValue := []interface{}{"1"}
		checkExpression(t, got, want, wantValue)
	})
	t.Run("test of Andclause", func(t *testing.T) {
		got := &Expression{}
		clause := "id = 1"
		err := got.AndClause(clause)
		if err != nil {
			t.Error(err)
		}
		want := "id = ?"
		wantValue := []interface{}{"1"}
		checkExpression(t, got, want, wantValue)
	})
	t.Run("test of Orclause", func(t *testing.T) {
		got := &Expression{}
		clause := "id = 1"
		err := got.OrClause(clause)
		if err != nil {
			t.Error(err)
		}
		want := "id = ?"
		wantValue := []interface{}{"1"}
		checkExpression(t, got, want, wantValue)
	})
}
